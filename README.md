<div align="center">
<h1>SFTP4CJ</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.13-brightgreen" style="display: inline-block;" />
</p>

## 介绍

**SFTP4CJ** 是一个基于 SSH 协议的 SFTP 服务器和客户端实现。支持文件上传、下载、目录操作等基本功能。

### 项目特性

- 支持 SSH 协议的 SFTP 服务器
- 支持 SFTP 客户端操作
- 支持文件上传下载
- 支持目录创建删除
- 支持文件权限管理

### 项目架构

主要包含以下模块:

- `SshServer` - SSH 服务器实现
- `SftpClient` - SFTP 客户端实现 
- `SftpPacket` - SFTP 协议包处理
- `VersionManager` - SFTP 版本管理

### 源码目录

```shell
.
├── build.cj               # 构建脚本
├── cjpm.toml             # 项目配置
├── src                   # 源码目录
    ├── client           # 客户端实现
    ├── server          # 服务器实现  
    ├── packet          # 协议包处理
    ├── version         # 版本管理
    └── test            # 测试用例
```

### 使用说明

#### 环境准备

1. Windows 环境需要:
   - 以管理员权限运行命令行
   - 预先安装 OpenSSH2 库
   ```bash
   # 使用 vcpkg 安装
   vcpkg install libssh2:x64-windows
   ```

2. Linux 环境需要:
   ```bash
   # Ubuntu/Debian
   sudo apt-get install libssh2-1-dev
   
   # CentOS/RHEL
   sudo yum install libssh2-devel
   ```

#### 使用示例

1. 启动服务器:

```cj
let server = SshServer(22u16) 
server.start()
```

2. 客户端连接:

```cj
let client = SftpClient("127.0.0.1", 22u16, Duration.second * 5, 3u32)
client.connect()
```

3. 文件操作:

```cj
// 打开文件
let handle = client.openFile("test.txt", OpenFlags.WRITE | OpenFlags.CREATE, 0u32)

// 写入文件
client.writeFile(handle, 0u64, data)

// 关闭文件
client.closeFile(handle)
```

4. 目录操作:

```cj
// 创建目录
client.mkDir("parent")

// 删除目录
client.rmDir("parent")
```

5. 中文文件支持:

```cj
// 创建中文文件
let handle = client.openFile("测试文件.txt", 
    OpenFlags.WRITE | OpenFlags.CREATE | OpenFlags.TRUNCATE, 0u32)

// 写入中文内容
let content = "你好，世界！".toArray()
client.writeFile(handle, 0u64, content)
client.closeFile(handle)
```

6. 文件权限管理:

```cj
// 创建文件并设置权限为 0644 (rw-r--r--)
let handle = client.openFile("perm_test.txt", 
    OpenFlags.WRITE | OpenFlags.CREATE | OpenFlags.TRUNCATE, 0u32)
client.closeFile(handle)

let permAttrs = Array<UInt8>(4, item: 0)
let setMode = 0o644u32
permAttrs[0] = UInt8((setMode >> 24) & 0xFF)
permAttrs[1] = UInt8((setMode >> 16) & 0xFF)
permAttrs[2] = UInt8((setMode >> 8) & 0xFF)
permAttrs[3] = UInt8(setMode & 0xFF)

let setPermPacket = SftpPacket.makeSetstat(94u32, "perm_test.txt", 0x00000004u32, permAttrs)
```

7. 关闭连接:

```cj
client.close()
```

## 约束与限制

- 支持仓颉版本 `0.53.13`
- 需要系统支持 SSH 协议
- 目前支持 SFTP 协议版本 3
- windows适配不完全

## 开源协议

[MIT License](LICENSE)

## 参与贡献

欢迎提交 PR 和 Issue。本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 培育。
This project is supervised by @zhangyin-gitcode.

## 参考项目

- [net-sftp](https://github.com/net-ssh/net-sftp) - Ruby 语言实现的 SFTP 客户端